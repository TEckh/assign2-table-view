//
//  InformationViewController.m
//  NavigationTable
//
//  Created by Adam Hellewell on 5/19/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//
//  Modified by Trevor Eckhardt on 5/21/15
//

#import "InformationViewController.h"

@interface InformationViewController ()

@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat height = self.view.frame.size.height - 65;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel* nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 30)];
    nameLbl.text = [self.informationDictionary objectForKey:@"name"];
    nameLbl.textAlignment = NSTextAlignmentCenter;
    nameLbl.textColor = [UIColor redColor];
    [self.view addSubview:nameLbl];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 65 + (height * 5/48), self.view.frame.size.width, (height * 18/48))];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [iv setImage:[UIImage imageNamed: [self.informationDictionary objectForKey:@"image"]]];
    [self.view addSubview:iv];
    
    UITextView* labelDesc = [[UITextView alloc]initWithFrame:CGRectMake(0, 65 + (height * 24/48), self.view.frame.size.width, (height * 23/48))];
    labelDesc.text = [self.informationDictionary objectForKey:@"description"];
    labelDesc.textAlignment = NSTextAlignmentLeft;
    labelDesc.textColor = [UIColor blackColor];
    labelDesc.editable = NO;
    [self.view addSubview:labelDesc];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
