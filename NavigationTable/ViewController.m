//
//  ViewController.m
//  NavigationTable
//
//  Created by Adam Hellewell on 5/19/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//
//  Modified by Trevor Eckhardt on 05/21/15
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayOfThings = @[
                      @{
                         @"name":@"Slow Cooker Barbeque",
                         @"image":@"SlowCookerBBQPork",
                         @"description":@"Ingredients:\n1 (3 pound) boneless chuck roast\n1 teaspoon garlic powder\n1 teaspoon onion powder\nsalt and pepper to taste\n1 (18 ounce) bottle barbeque sauce\n\nDirections:\n1 Place roast into slow cooker. Sprinkle with garlic powder and onion powder, and season with salt and pepper. Pour barbeque sauce over meat. Cook on Low for 6 to 8 hours.\n2 Remove meat from slow cooker, shred, and return to slow cooker. Cook for 1 more hour. Serve hot."
                          },
                      @{
                          @"name":@"Slow Cooker Pizza",
                          @"image":@"SlowCookerPizza",
                          @"description":@"Ingredients:\n1 1/2 pounds ground beef\n1 (8 ounce) package rigatoni pasta\n1 (16 ounce) package shredded mozzarella cheese\n1 (10.75 ounce) can condensed cream of tomato soup\n2 (14 ounce) jars pizza sauce/n1(8 ounce) package slice pepperoni sausage\n\nDirections:\n1 Bring a large pot of lightly salted water to a boil. Add pasta and cook for 8 to 10 minutes or until al dente; drain and set aside. Brown the ground beef in a skillet over medium-high heat. Drain off grease.\n2 In slow cooker, alternate layers of ground beef, noodles, cheese, soup, sauce and pepperoni.\n3 Cook on Low setting for 4 hours."
                          },
                      @{
                          @"name":@"Slow Cooker Tapioca Pudding",
                          @"image":@"SlowCookerTapioca",
                          @"description":@"Ingredients:\n4 cups milk\n2/3 cup white sugar\n1/2 cup small pearl tapioca\n2 eggs, lightly beaten\n\nDirections:\n1 Stir together the milk, sugar, tapioca, and eggs in a slow cooker. Cover, and cook on Medium for 3 hours, or on Low for 6 hours, stirring occasionally. Serve warm."
                          },
                      @{
                          @"name":@"Slow Cooker Enchiladas",
                          @"image":@"SlowCookerEnchiladas",
                          @"description":@"Ingredients:\n1 pound lean ground beef\n10 (6 inch) corn tortillas, quartered\n1 (1 ounce) package taco seasoning mix\n1 1/4 cups water\n1 (12 ounce) jar chunky salsa\n1 (10.75 ounce) can condensed cream of mushroom soup\n1 (10.75 ounce) can condensed cream of chicken soup\n4 cups shredded Mexican cheese blend\n\n Directions:\n1 Crumble the ground beef into a skillet over medium-high heat. Cook and stir until evenly browned. Add taco seasoning and water; simmer for 15 minutes over low heat.\n2 In a medium bowl, stir together the salsa, cream of mushroom soup and cream of chicken soup. Mix in most of the cheese, reserving 3/4 cup for later.\n3 Place a layer of tortillas covering the bottom of a slow cooker. Scoop a layer of the ground beef over that, and then spoon a layer of the cheese mixture. Repeat the layers until you run out of stuff, ending with a layer of tortillas on the top. Top with remaining cheese.\n4 Cover, and cook on High for 45 minutes to 1 hour."
                          },
                      @{
                          @"name":@"Slow Cooker Chicken and Dumplings",
                          @"image":@"SlowCookerChickenDumplings",
                          @"description":@"Ingredients:\n4 skinless, coneless chicken breast halves\n2 tablespoons butter\n2 (10.75 ounce) cans condensed cream of chicken soup\n1 onion, finely diced\n2 (10 ounce) packages refrigerated biscuit dough, torn into pieces\n\nDirections:\n1 Place the chicken, butter, soup, and onion in a slow cooker, and fill with enough water to cover.\n2 Cover, and cook for 5 to 6 hours on High. About 30 minutes before serving, place the torn biscuit dough in the slow cooker. Cook until the dough is no longer raw in the center."
                          }
                      ];
    
//    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
//    [btn setFrame:CGRectMake(100, 100, 100, 100)];
//    [btn setTitle:@"Touch me" forState:UIControlStateNormal];
//    [btn addTarget:self action:@selector(btnTouched:) forControlEvents:UIControlEventTouchUpInside];
//    [btn setBackgroundColor:[UIColor redColor]];
//    [self.view addSubview:btn];
    
    UITableView* tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayOfThings.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* infoDictionary = arrayOfThings[indexPath.row];
    
    cell.textLabel.text = [infoDictionary objectForKey:@"name"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* infoDictionary = arrayOfThings[indexPath.row];
    
    InformationViewController* info = [InformationViewController new];
    info.informationDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
}

-(void)btnTouched:(UIButton*)btn {
    InformationViewController* info = [InformationViewController new];
    [self.navigationController pushViewController:info animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
