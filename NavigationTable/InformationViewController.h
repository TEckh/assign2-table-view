//
//  InformationViewController.h
//  NavigationTable
//
//  Created by Adam Hellewell on 5/19/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationViewController : UIViewController


@property (nonatomic, strong) NSDictionary* informationDictionary;

@end
