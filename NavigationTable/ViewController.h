//
//  ViewController.h
//  NavigationTable
//
//  Created by Adam Hellewell on 5/19/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InformationViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* arrayOfThings;
}

@end

