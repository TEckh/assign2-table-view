//
//  main.m
//  NavigationTable
//
//  Created by Adam Hellewell on 5/19/15.
//  Copyright (c) 2015 Adam Hellewell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
